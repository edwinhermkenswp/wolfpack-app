class strings_NL{
  /// default language
  static String appName = "The pack app";

  /// wolfpack slang
  static String wolf = "Wolf";
  static String pup = "Pup";

  /// default language
  static String ok = "Ok";
  static String yes = "Yes";
  static String no = "No";

  /// navigation
  static const String navigation_bottombar_home = "Home";
  static const String navigation_bottombar_timeline = "Timeline";
  static const String navigation_bottombar_profile = "Profile";

  /// button
  static String button_positive = "Ok";
  static String button_negative = "No";

}