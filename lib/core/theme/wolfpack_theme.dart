import 'dart:io';
import 'package:flutter/material.dart';

class WolfpackColors {
  WolfpackColors._();

  /// Wolfpack primary colors
  static const primary = Color(0xffbb0000);
  static const wolfpackRed =  Color(0xffbb0000);
  static const wolfpackGrey = Color(0xff54595f);
  static const wolfpackBlue =  Color(0xff00A2FF);
  static const textAccent = Color(0xb2b00000);

  ///error handling colors
  static const errorDefault = Color(0xFFBF1341);
  static const negative = Color(0xFFB02B44);

  /// Several colors
  static const accentOrange = Color(0xFFFF9C3D);
  static const signalYellow = Color(0xFFFFE450);
  static const positive = Color(0xFF00875A);
  static const signalGreenDark = Color(0xFF00875A);
  static const signalGreen = Color(0xFF6BE080);
  static const black = Color(0xFF000000);
  static const white = Color(0xFFFFFFFF);
  static const offWhite = Color(0xB9E5E5E5);

  /// Functional colors
  static const functionalRed = Color(0xFFEB0000);
  static const functionalOrange = Color(0xFFFF9C3D);
  static const functionalYellow = Color(0xFFFFE540);
  static const functionalGreen = Color(0xFF6BE080);
  static const functionalDarkGreen = Color(0xFF00875A);
  static const functionalBlue = Color(0xFF3E93FF);
  static const functionalDarkBlue = Color(0xFF0054D1);

  /// Other color settings
  static const indicator = wolfpackRed;
  static const fabBackground = wolfpackRed;
  static const icon = wolfpackRed;
  static const background = Color(0xFFFFFFFF);
  static const textSelection = wolfpackGrey;
  static const actionSheetTitle = Color(0xFF262626);
  static const divider = actionSheetTitle;
  static const hint = Color(0xFFC4C4C4);
  static const cardBackground = white;
}


    ThemeData buildWolfpackTheme() { //1
    final base = ThemeData.light();
    return base.copyWith( //2
        accentColor: WolfpackColors.wolfpackGrey,
        primaryColor: WolfpackColors.primary,
        textSelectionTheme: const TextSelectionThemeData(
          cursorColor: WolfpackColors.primary,
          selectionColor: WolfpackColors.textSelection,
          selectionHandleColor: WolfpackColors.textSelection,
        ),
        backgroundColor: WolfpackColors.background,
        indicatorColor: WolfpackColors.indicator,
        cardColor: WolfpackColors.cardBackground,
        hintColor: WolfpackColors.hint,
        errorColor: WolfpackColors.errorDefault,
        primaryColorDark: WolfpackColors.primary,
        scaffoldBackgroundColor: WolfpackColors.background,
        canvasColor: WolfpackColors.primary,
        highlightColor: WolfpackColors.primary,
        bottomAppBarTheme: _buildBottomAppBarTheme(base.bottomAppBarTheme),
        appBarTheme: _buildAppBarTheme(base.appBarTheme),
        unselectedWidgetColor: WolfpackColors.white,
        snackBarTheme: _snackBarThemeData(base.snackBarTheme),
        colorScheme: base.colorScheme.copyWith(primary: WolfpackColors.primary),
        buttonTheme: ButtonThemeData( // 4
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
          buttonColor: WolfpackColors.wolfpackBlue,
        )
    );
  }

  AppBarTheme _buildAppBarTheme(AppBarTheme base) {
    return base.copyWith(
        //color: Colors.transparent,
        elevation: 0,
        brightness: Platform.isAndroid ? Brightness.dark : Brightness.light,
        textTheme: const TextTheme(
          headline1: TextStyle(
            color: WolfpackColors.primary,
            fontSize: 18,
            fontFamily: 'WorkSans_medium',
            fontWeight: FontWeight.w700,
            height: 2,
            letterSpacing: 0,
          ),
        ),
        centerTitle: true,
        iconTheme: const IconThemeData(color: WolfpackColors.primary),
        actionsIconTheme: const IconThemeData(
          color: WolfpackColors.primary,
        ));
  }

BottomAppBarTheme _buildBottomAppBarTheme(BottomAppBarTheme base) {
  return base.copyWith(
      elevation: 0,
      color: WolfpackColors.primary,
  );
}

  SnackBarThemeData _snackBarThemeData(SnackBarThemeData base) {
    return base.copyWith(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        backgroundColor: WolfpackColors.black,
        behavior: SnackBarBehavior.floating,
        contentTextStyle: const TextStyle(
          color: WolfpackColors.white,
          fontSize: 14,
          fontFamily: 'WorkSans_medium',
          fontWeight: FontWeight.w500,
          height: 1.5,
          letterSpacing: 0,
        ),
        actionTextColor: WolfpackColors.background);
  }



