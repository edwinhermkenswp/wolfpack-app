abstract class StorageService{
  static String DARKMODE_BOOL_KEY = "darkmode_bool_key";
  static String BOTTOMBAR_SELECTED_ITEM_INT_KEY = "bottombar_selecte_idem_int_key";

  Future<void> init();

  bool getDarkMode();
  void saveDarkMode(bool dark);

  int getSelectedBottombarItem();
  void saveSelectedBottombarItem(int index);
}