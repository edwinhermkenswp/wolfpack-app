abstract class AnalyticsService{
  Future<void> logEvent(String eventName, { Map<String, dynamic>? args});
  Future<void> setProperty({required String key, String? value});
}
