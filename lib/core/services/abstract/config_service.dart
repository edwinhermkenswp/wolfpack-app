abstract class ConfigService{
  static String CONFIG_STRING_GROUP = "config_group";

  Future<void> init();
  String getGroup();
}