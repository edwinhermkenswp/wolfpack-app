import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:wolfpack/core/services/abstract/analytics_service.dart';

class AnalyticsServiceFirebase extends AnalyticsService{
  AnalyticsServiceFirebase();


  final FirebaseAnalytics _firebase = FirebaseAnalytics();

  @override
  Future<void> logEvent(String eventName, { Map<String, dynamic>? args}) async{
    args ??= {};
    await _firebase.logEvent(
      name: eventName,
      parameters: <String, dynamic>{
        'testkey': 'testvalue',
      },
    );
  }

  @override
  Future<void> setProperty({required String key, String? value}) async {
    _firebase.setUserProperty(name: key, value: value);
  }

}