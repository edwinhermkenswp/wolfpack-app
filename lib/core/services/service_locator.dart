import 'package:wolfpack/core/services/abstract/analytics_service.dart';
import 'package:wolfpack/core/services/abstract/config_service.dart';
import 'package:wolfpack/core/services/abstract/messaging_service.dart';
import 'package:wolfpack/core/services/analytis_service_firebase.dart';
import 'package:wolfpack/core/services/config_service_firebase.dart';
import 'package:wolfpack/core/services/messaging_service_firebase.dart';
import 'package:wolfpack/core/services/storage_service_sharedpref.dart';

import 'storage_service_sharedpref.dart';
import 'package:get_it/get_it.dart';
import 'abstract/storage_service.dart';

final getIt = GetIt.instance; //this is a global variable we can access everywhere across the code

setupServiceLocator() {
  //register lazy locators (only when first used)
  getIt.registerLazySingleton<StorageService>(() => StorageServiceSharedPref());
  getIt.registerLazySingleton<ConfigService>(() => ConfigServiceFirebase());
  getIt.registerLazySingleton<MessagingService>(() => MessagingServiceFirebase());
  getIt.registerLazySingleton<AnalyticsService>(() => AnalyticsServiceFirebase());

  //register regular locators (init always)
}
