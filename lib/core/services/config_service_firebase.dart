import 'package:wolfpack/core/services/abstract/config_service.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

class ConfigServiceFirebase extends ConfigService {
  final RemoteConfig _remoteConfig = RemoteConfig.instance;


  Future<void> init() async {
    try {
      await _remoteConfig.setConfigSettings(RemoteConfigSettings(
        fetchTimeout: const Duration(seconds: 10),
        minimumFetchInterval: const Duration(hours: 1),
      ));
      await _remoteConfig.setDefaults(<String, dynamic>{
        'group': 'not_set',
      });
      RemoteConfigValue(null, ValueSource.valueStatic);
      await _remoteConfig.fetchAndActivate();
    } on Exception catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
      print(exception);
    }
  }

  @override
  String getGroup()  {
    return  _remoteConfig.getString(ConfigService.CONFIG_STRING_GROUP);
  }
}
