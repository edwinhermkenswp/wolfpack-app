import 'package:wolfpack/core/services/abstract/storage_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageServiceSharedPref extends StorageService{
  late SharedPreferences _prefs;

  @override
  Future<void> init() async{
    _prefs = await SharedPreferences.getInstance();
  }

  @override
  bool getDarkMode()  {
    return _prefs.getBool(StorageService.DARKMODE_BOOL_KEY) ?? false;
  }

  @override
   void saveDarkMode(bool value)  {
    _prefs.setBool(StorageService.DARKMODE_BOOL_KEY, value);
  }

  @override
  int getSelectedBottombarItem() {
    return _prefs.getInt(StorageService.BOTTOMBAR_SELECTED_ITEM_INT_KEY) ?? 0;
  }

  @override
  void saveSelectedBottombarItem(int index) {
    _prefs.setInt(StorageService.BOTTOMBAR_SELECTED_ITEM_INT_KEY, index);
  }


  
}