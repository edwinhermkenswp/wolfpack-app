import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:wolfpack/components/organisms/message_card_organism.dart';
import 'package:wolfpack/models/Message.dart';

class TimelinePage extends StatefulWidget {
  @override
  _TimelinePageState createState() => _TimelinePageState();
}

class _TimelinePageState extends State<TimelinePage> {
  late CollectionReference messages = FirebaseFirestore.instance.collection('messages');

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder<DocumentSnapshot>(
          future: messages.doc('YgqiO2rUUiqi9tjXDPBK').get(),//Get the data from cloud firestore
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              if (snapshot.hasError) {
                //showSnackbar("Something went wrong");
              }
              if (snapshot.connectionState == ConnectionState.done) {
                Map<String, dynamic> data = snapshot.data!.data() as Map<String, dynamic>;
                //showSnackbar("Full Name: ${data['first_name']} ${data['last_name']}");
                Message message =  Message(data['title'],data['subtitle'],data['message'], data['vogtes']);
                return ListView(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  children: [
                    MessageCardOrganism(message: message),
                  ],
                );
              }
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),

      ],
    );
  }
}
