import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:wolfpack/components/atoms/textfield_atom_widget.dart';
import 'package:wolfpack/components/organisms/profile_image_organism_widget.dart';
import 'package:wolfpack/models/User.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  User user = new User();

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 32),
      physics: BouncingScrollPhysics(),
      children: [
        ProfileImageOrganismWidget(
          imagePath: user.imagePath,
          isEdit: true,
          onClicked: () async {},
        ),
        const SizedBox(height: 24),
        TextFieldAtomWidget(
          label: 'First Name',
          text: user.firstName,
          onChanged: (name) {},
        ),
        const SizedBox(height: 24),
        TextFieldAtomWidget(
          label: 'Email',
          text: user.email,
          onChanged: (email) {},
        ),
        const SizedBox(height: 24),
        TextFieldAtomWidget(
          label: 'About',
          text: user.about,
          maxLines: 5,
          onChanged: (about) {},
        ),
      ],
    );
  }
}