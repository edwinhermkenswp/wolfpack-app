import 'package:flutter/material.dart';
import 'package:wolfpack/components/atoms/button_atom_widget.dart';
import 'package:wolfpack/components/organisms/profile_specs_organism.dart';
import 'package:wolfpack/components/organisms/profile_image_organism_widget.dart';
import 'package:wolfpack/models/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'edit_profile_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}



class _ProfilePageState extends State<ProfilePage> {
  late CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    User user = new User();
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      FutureBuilder<DocumentSnapshot>(
        future: users.doc('OY0NGaKcqM7rr1QrKohN').get(),//Get the data from cloud firestore
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            if (snapshot.hasError) {
              //showSnackbar("Something went wrong");
            }
            if (snapshot.connectionState == ConnectionState.done) {
              Map<String, dynamic> data = snapshot.data!.data() as Map<String, dynamic>;
              //showSnackbar("Full Name: ${data['first_name']} ${data['last_name']}");
              user.firstName = data['first_name'];
              user.lastName = data['last_name'];
              user.email = data['email'];
              user.ranking = data['ranking'];
              user.projects = data['projects'];
              user.skills = data['skills'];
              user.about = data['about'];
              return ListView(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                children: [
                  ProfileImageOrganismWidget(
                    imagePath: user.imagePath,
                    onClicked: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => EditProfilePage()),
                      );
                    },
                  ),
                  const SizedBox(height: 24),
                  buildName(user),
                  const SizedBox(height: 24),
                  Center(child: buildGiveAHowlButton()),
                  const SizedBox(height: 24),
                  ProfileSpecsOrganismWidget(ranking: user.ranking,projects: user.projects, skills: user.skills),
                  const SizedBox(height: 48),
                  buildAbout(user),
                ],
              );
            }
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    ],
    );
  }

  Widget buildName(User user) => Column(
        children: [
          Text(
            user.firstName,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            user.email,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildGiveAHowlButton() => ButtonAtomWidget(
        text: 'Give a Howl',
        onClicked: () {},
      );

  Widget buildAbout(User user) => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'About this Wolf',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            Text(
              user.about,
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      );
}
