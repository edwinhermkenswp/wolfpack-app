import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:wolfpack/core/services/service_locator.dart';
import 'package:wolfpack/core/services/abstract/storage_service.dart';
import 'package:wolfpack/core/theme/wolfpack_theme.dart';
import 'package:wolfpack/features/profile/profile_page.dart';
import 'package:wolfpack/features/timeline/timeline_page.dart';
import '../core/strings/strings_nl.dart';
import 'dashboard/dashboard.dart';

class Home extends StatefulWidget {
  const Home({
    Key? key,
    required this.title,
  }) : super(key: key);


  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  late StorageService _storageService ;
  int _selectedIndex = 0;

  @override
  void initState(){
    super.initState();
    _storageService = getIt<StorageService>();
  }

  void _clickFAB() {
    setState(() {
      //nothing
    });
  }

  void _onBottomBarItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


   void showSnackbar(String message){
    var snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: strings_NL.button_positive,
        onPressed: () {
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  final List<StatefulWidget> _pages = <StatefulWidget>[
    Dashboard(),
    TimelinePage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: _pages[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: WolfpackColors.offWhite,
        selectedItemColor: WolfpackColors.white,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: strings_NL.navigation_bottombar_home,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timeline),
            label: strings_NL.navigation_bottombar_timeline,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_rounded),
            label: strings_NL.navigation_bottombar_profile,
          ),
        ],
        onTap: _onBottomBarItemTapped,
        currentIndex: _selectedIndex,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _clickFAB,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }

}
