import 'package:flutter/material.dart';
import 'package:wolfpack/models/Message.dart';

class MessageCardOrganism extends StatelessWidget {
  final Message message;

  const MessageCardOrganism({
    Key? key,
    required this.message,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return  Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
           ListTile(
            leading: Icon(Icons.warning_amber),
            title: Text(message.title),
            subtitle: Text(message.subtitle),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              TextButton(
                child: const Text("proceed"),
                onPressed: () {/* ... */},
              ),
              const SizedBox(width: 8),
            ],
          ),
        ],
      ),
    );
  }


}
