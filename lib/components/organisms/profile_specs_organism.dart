import 'package:flutter/material.dart';

class ProfileSpecsOrganismWidget extends StatelessWidget {
  final String ranking;
  final String projects;
  final String skills;

  const ProfileSpecsOrganismWidget({
    Key? key,
    required this.ranking,
    required this.projects,
    required this.skills,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      buildButton(context, ranking, 'Ranking'),
      buildDivider(),
      buildButton(context, projects, 'Projects'),
      buildDivider(),
      buildButton(context, skills, 'Skills'),
    ],
  );
  Widget buildDivider() => Container(
    height: 24,
    child: VerticalDivider(),
  );

  Widget buildButton(BuildContext context, String value, String text) =>
      MaterialButton(
        padding: EdgeInsets.symmetric(vertical: 4),
        onPressed: () {},
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              value,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
            SizedBox(height: 2),
            Text(
              text,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      );
}