import 'package:flutter/material.dart';
import 'package:wolfpack/core/strings/strings_nl.dart';
import 'package:wolfpack/core/theme/wolfpack_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'features/home.dart';
import 'core/services/service_locator.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  setupServiceLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: strings_NL.appName,
      theme: buildWolfpackTheme(),
      home: Home(title: strings_NL.appName),
    );
  }
}

